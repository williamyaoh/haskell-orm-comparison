module Main where

import Control.Monad.Logger
import Control.Monad.IO.Class

import Database.PostgreSQL.Simple as PG
import Database.Persist.Postgresql as PG

import Lib

connInfo :: PG.ConnectInfo
connInfo = PG.ConnectInfo
  { connectPort = 5432
  , connectHost = "localhost"
  , connectDatabase = "hitmen"
  , connectUser = "postgres"
  , connectPassword = ""
  }

main :: IO ()
main = runStderrLoggingT $
  PG.withPostgresqlConn (postgreSQLConnectionString connInfo) $ \conn -> do
    handlers <- runSqlConn allHandlers conn
    hitmen <- runSqlConn allHitmen conn
    marks <- runSqlConn allMarks conn
    pursuingMarks <- runSqlConn allPursuingMarks conn
    erasedMarks <- runSqlConn allErasedMarks conn

    liftIO $ print handlers
    liftIO $ print hitmen
    liftIO $ print marks
    liftIO $ print pursuingMarks
    liftIO $ print erasedMarks

    latest <- runSqlConn latestHits conn
    totalBountyAwarded1 <- runSqlConn (totalBountyAwarded (HitmanKey 1)) conn

    liftIO $ putStrLn "===== LATEST HITS ====="
    liftIO $ print latest

    liftIO $ putStrLn "===== TOTAL BOUNTY AWARDED TO HITMAN 1 ====="
    liftIO $ print totalBountyAwarded1
