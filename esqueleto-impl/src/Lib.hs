{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib where

import Data.Fixed
import Data.Int
import Data.Text ( Text )
import Data.Time

import Control.Monad.IO.Class

import Database.Persist.TH
import Database.Esqueleto

share [mkPersist sqlSettings] [persistLowerCase|
Handler sql=handlers
  codename Text
  createdAt UTCTime
  updatedAt UTCTime
  deriving Show
Hitman sql=hitmen
  codename Text
  handlerId HandlerId
  createdAt UTCTime
  updatedAt UTCTime
  deriving Show
Mark sql=marks
  listBounty Int64
  firstName Text
  lastName Text
  description Text Maybe
  createdAt UTCTime
  updatedAt UTCTime
  deriving Show
PursuingMark sql=pursuing_marks
  hitmanId HitmanId
  markId MarkId
  createdAt UTCTime
  updatedAt UTCTime
  Primary hitmanId markId
  deriving Show
ErasedMark sql=erased_marks
  hitmanId HitmanId
  markId MarkId
  awardedBounty Int64
  createdAt UTCTime
  updatedAt UTCTime
  Primary hitmanId markId
  deriving Show
|]

mkUTCTime :: (Integer, Int, Int)
          -> (Int, Int, Pico)
          -> UTCTime
mkUTCTime (year, mon, day) (hour, min, sec) =
  UTCTime (fromGregorian year mon day)
          (timeOfDayToTime (TimeOfDay hour min sec))

--------------------
-- QUERIES
--------------------

allHandlers :: MonadIO m => SqlPersistT m [Entity Handler]
allHandlers = select $ from pure

allHitmen :: MonadIO m => SqlPersistT m [Entity Hitman]
allHitmen = select $ from pure

allMarks :: MonadIO m => SqlPersistT m [Entity Mark]
allMarks = select $ from pure

allPursuingMarks :: MonadIO m => SqlPersistT m [Entity PursuingMark]
allPursuingMarks = select $ from pure

allErasedMarks :: MonadIO m => SqlPersistT m [Entity ErasedMark]
allErasedMarks = select $ from pure

activelyPursuingMarks :: MonadIO m => SqlPersistT m [Entity Hitman]
activelyPursuingMarks = select $
  from $ \(hitman `InnerJoin` pmark `LeftOuterJoin` emark) ->
    distinctOn [don (hitman ^. HitmanId)] $ do
      on (hitman ^. HitmanId ==. pmark ^. PursuingMarkHitmanId)
      on (just (pmark ^. PursuingMarkMarkId) ==. emark ?. ErasedMarkMarkId)
      where_ (isNothing $ emark ?. ErasedMarkHitmanId)
      pure hitman

erasedSince :: MonadIO m => UTCTime -> SqlPersistT m [Entity Mark]
erasedSince since = select $
  from $ \(mark, emark) -> do
    where_ (mark ^. MarkId ==. emark ^. ErasedMarkMarkId)
    where_ (emark ^. ErasedMarkCreatedAt >=. val since)
    pure mark

erasedSinceBy :: MonadIO m => UTCTime -> HitmanId -> SqlPersistT m [Entity Mark]
erasedSinceBy since by = select $
  from $ \(mark, emark) -> do
    where_ (mark ^. MarkId ==. emark ^. ErasedMarkMarkId)
    where_ (emark ^. ErasedMarkCreatedAt >=. val since)
    where_ (emark ^. ErasedMarkHitmanId ==. val by)
    pure mark

-- | We make sure to return 0 for any hitmen who don't have any
--   bounties awarded, rather than omitting them entirely.
totalBountiesAwarded :: MonadIO m => SqlPersistT m [(Entity Hitman, Value Rational)]
totalBountiesAwarded = select $
  from $ \(hitman `LeftOuterJoin` emark) -> do
    on (emark ?. ErasedMarkHitmanId ==. just (hitman ^. HitmanId))
    groupBy (hitman ^. HitmanId)
    orderBy [asc (hitman ^. HitmanId)]
    let mbounty = coalesceDefault [emark ?. ErasedMarkAwardedBounty] (val 0)
    pure ( hitman
         , coalesceDefault [sum_ mbounty] (val 0)
         )

totalBountyAwarded :: MonadIO m => HitmanId -> SqlPersistT m [(Value Rational)]
totalBountyAwarded hid = select $
  from $ \(hitman `LeftOuterJoin` emark) -> do
    on (emark ?. ErasedMarkHitmanId ==. just (hitman ^. HitmanId))
    where_ (hitman ^. HitmanId ==. val hid)
    let mbounty = coalesceDefault [emark ?. ErasedMarkAwardedBounty] (val 0)
    pure (coalesceDefault [sum_ mbounty] (val 0))

latestHits :: MonadIO m => SqlPersistT m [(Entity Hitman, Maybe (Entity Mark))]
latestHits = select $
  from $ \(hitman `LeftOuterJoin` emark1 `LeftOuterJoin` emark2 `LeftOuterJoin` mark) -> do
    on (emark1 ?. ErasedMarkHitmanId ==. emark2 ?. ErasedMarkHitmanId &&.
        emark1 ?. ErasedMarkCreatedAt <. emark2 ?. ErasedMarkCreatedAt &&.
        emark1 ?. ErasedMarkMarkId >. emark2 ?. ErasedMarkMarkId)
    on (emark1 ?. ErasedMarkHitmanId ==. just (hitman ^. HitmanId))
    on (emark1 ?. ErasedMarkMarkId ==. mark ?. MarkId)
    where_ (isNothing $ emark2 ?. ErasedMarkCreatedAt)
    where_ (isNothing $ emark2 ?. ErasedMarkMarkId)
    pure (hitman, mark)

latestHit :: MonadIO m => HitmanId -> SqlPersistT m [Maybe (Entity Mark)]
latestHit hid = select $
  from $ \(hitman `LeftOuterJoin` emark1 `LeftOuterJoin` emark2 `LeftOuterJoin` mark) -> do
    on (emark1 ?. ErasedMarkHitmanId ==. emark2 ?. ErasedMarkHitmanId &&.
        emark1 ?. ErasedMarkCreatedAt <. emark2 ?. ErasedMarkCreatedAt &&.
        emark1 ?. ErasedMarkMarkId >. emark2 ?. ErasedMarkMarkId)
    on (emark1 ?. ErasedMarkHitmanId ==. just (hitman ^. HitmanId))
    on (emark1 ?. ErasedMarkMarkId ==. mark ?. MarkId)
    where_ (isNothing $ emark2 ?. ErasedMarkCreatedAt)
    where_ (isNothing $ emark2 ?. ErasedMarkMarkId)
    where_ (hitman ^. HitmanId ==. val hid)
    pure mark

singularPursuer :: MonadIO m => SqlPersistT m [(Value HitmanId, Value MarkId)]
singularPursuer = select $
  from $ \(hitman `InnerJoin` pmark `InnerJoin` mark `LeftOuterJoin` emark) -> do
    on (pmark ^. PursuingMarkHitmanId ==. hitman ^. HitmanId)
    on (pmark ^. PursuingMarkMarkId ==. mark ^. MarkId)
    on (just (pmark ^. PursuingMarkMarkId) ==. emark ?. ErasedMarkMarkId)
    where_ (isNothing $ emark ?. ErasedMarkMarkId)
    groupBy (mark ^. MarkId)
    having (count (pmark ^. PursuingMarkHitmanId) ==. val (1 :: Int64))
    -- the max should never be null
    pure ( coalesceDefault [max_ (hitman ^. HitmanId)] (val (HitmanKey 0))
         , mark ^. MarkId
         )

marksOfOpportunity :: MonadIO m => SqlPersistT m [(Entity Hitman, Entity Mark)]
marksOfOpportunity = select $
  from $ \(emark `InnerJoin` hitman `InnerJoin` mark `LeftOuterJoin` pmark) -> do
    on (emark ^. ErasedMarkHitmanId ==. hitman ^. HitmanId)
    on (emark ^. ErasedMarkMarkId ==. mark ^. MarkId)
    on (just (emark ^. ErasedMarkHitmanId) ==. pmark ?. PursuingMarkHitmanId &&.
        just (emark ^. ErasedMarkMarkId) ==. pmark ?. PursuingMarkMarkId)
    where_ (isNothing $ pmark ?. PursuingMarkMarkId)
    pure (hitman, mark)

--------------------
-- INSERTS/UPDATES
--------------------

-- The seed insertions are just done through Persistent.
-- However, I can't figure out if there's a way to use SQL DEFAULT.

insertSeedHandlers :: MonadIO m => SqlPersistT m ()
insertSeedHandlers =
  let midnight = mkUTCTime (2019, 1, 1) (0, 0, 0)
  in insertMany_
    [ Handler "Olive" midnight midnight
    , Handler "Pallas" midnight midnight
    ]

insertSeedHitmen :: MonadIO m => SqlPersistT m ()
insertSeedHitmen =
  let midnight = mkUTCTime (2019, 1, 1) (0, 0, 0)
  in insertMany_
    [ Hitman "Callaird" (HandlerKey 1) midnight midnight
    , Hitman "Bomois" (HandlerKey 1) midnight midnight
    , Hitman "Dune" (HandlerKey 2) midnight midnight
    ]

insertSeedMarks :: MonadIO m => SqlPersistT m ()
insertSeedMarks =
  let midnight = mkUTCTime (2019, 1, 1) (0, 0, 0)
  in insertMany_
    [ Mark 25000 "John" "Tosti" Nothing midnight midnight
    , Mark 50000 "Macie" "Jordan" Nothing midnight midnight
    , Mark 33000 "Sal" "Aspot" Nothing midnight midnight
    , Mark 10000 "Lars" "Andersen" Nothing midnight midnight
    ]

insertSeedPursuingMarks :: MonadIO m => SqlPersistT m ()
insertSeedPursuingMarks = insertMany_
  [ PursuingMark (HitmanKey 1) (MarkKey 2) (mkUTCTime (2018, 7, 1) (0, 0, 0)) (mkUTCTime (2019, 1, 1) (0, 0, 0))
  , PursuingMark (HitmanKey 2) (MarkKey 2) (mkUTCTime (2018, 7, 2) (0, 0, 0)) (mkUTCTime (2019, 1, 1) (0, 0, 0))
  , PursuingMark (HitmanKey 2) (MarkKey 4) (mkUTCTime (2019, 5, 5) (0, 0, 0)) (mkUTCTime (2019, 1, 1) (0, 0, 0))
  , PursuingMark (HitmanKey 3) (MarkKey 3) (mkUTCTime (2018, 5, 13) (0, 0, 0)) (mkUTCTime (2019, 1, 1) (0, 0, 0))
  , PursuingMark (HitmanKey 3) (MarkKey 2) (mkUTCTime (2019, 2, 15) (0, 0, 0)) (mkUTCTime (2019, 1, 1) (0, 0, 0))
  ]

insertSeedErasedMarks :: MonadIO m => SqlPersistT m ()
insertSeedErasedMarks = insertMany_
  [ ErasedMark (HitmanKey 1) (MarkKey 2) 30000 (mkUTCTime (2018, 9, 3) (0, 0, 0)) (mkUTCTime (2019, 1, 1) (0, 0, 0))
  , ErasedMark (HitmanKey 1) (MarkKey 1) 55000 (mkUTCTime (2019, 2, 2) (0, 0, 0)) (mkUTCTime (2019, 1, 1) (0, 0, 0))
  , ErasedMark (HitmanKey 3) (MarkKey 3) 27000 (mkUTCTime (2018, 6, 30) (0, 0, 0)) (mkUTCTime (2019, 1, 1) (0, 0, 0))
  ]

increaseListBounty :: MonadIO m => Int64 -> MarkId -> SqlPersistT m ()
increaseListBounty amt mid = update $ \mark -> do
  set mark [ MarkListBounty +=. val amt ]
  where_ (mark ^. MarkId ==. val mid)
