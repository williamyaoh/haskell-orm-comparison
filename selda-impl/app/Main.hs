{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad.IO.Class ( MonadIO )
import Control.Monad.Catch ( MonadMask )

import Database.Selda
import Database.Selda.PostgreSQL

import Lib

connInfo :: PGConnectInfo
connInfo = PGConnectInfo
  { pgHost = "localhost"
  , pgPort = 5432
  , pgDatabase = "hitmen"
  , pgSchema = Nothing
  , pgUsername = Just "postgres"
  , pgPassword = Nothing
  }

main :: IO ()
main = withPostgreSQL connInfo $ do
  latest <- query latestHits
  totalBountyFor1 <- query (totalBountyAwarded (toId 1))

  increaseListBounty 1337 (toId 4)

  liftIO $ putStrLn "===== LATEST HITS ====="
  liftIO $ print latest

  liftIO $ putStrLn "===== TOTAL BOUNTY AWARDED TO HITMAN 1 ====="
  liftIO $ print totalBountyFor1
