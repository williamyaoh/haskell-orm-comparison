INSERT INTO handlers ( id, codename )
VALUES
  ( 1, 'Olive' ),
  ( 2, 'Pallas' );

INSERT INTO hitmen ( id, codename, handler_id )
VALUES
  ( 1, 'Callaird', 1 ),
  ( 2, 'Bomois', 1),
  ( 3, 'Dune', 2);

INSERT INTO marks ( id, first_name, last_name, list_bounty )
VALUES
  ( 1, 'John', 'Tosti', 25000 ),
  ( 2, 'Macie', 'Jordan', 50000 ),
  ( 3, 'Sal', 'Aspot', 33000 ),
  ( 4, 'Lars', 'Andersen', 10000 );

INSERT INTO pursuing_marks ( hitman_id, mark_id, created_at )
VALUES
  ( 1, 2, '2018-07-01' ),
  ( 2, 2, '2018-07-02' ),
  ( 2, 4, '2019-05-05' ),
  ( 3, 3, '2018-05-13' ),
  ( 3, 2, '2019-02-15' );

INSERT INTO erased_marks ( hitman_id, mark_id, awarded_bounty, created_at )
VALUES
  ( 1, 2, 30000, '2018-09-03' ),
  ( 1, 1, 55000, '2019-02-02' ),
  ( 3, 3, 27000, '2018-06-30' );
