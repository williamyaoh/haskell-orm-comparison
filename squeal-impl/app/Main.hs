{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Data.ByteString hiding ( putStrLn )

import Control.Monad.IO.Class ( liftIO )

import Squeal.PostgreSQL

import Lib

connstring :: ByteString
connstring = "host=localhost port=5432 dbname=hitmen user=postgres"

session :: PQ Schema Schema IO ()
session = do
  latest :: [HitInfo] <- runQuery latestHits >>= getRows
  totalBountyFor1 :: [BountyInfo] <- runQuery (totalBountyAwarded 1) >>= getRows

  manipulate_ (increaseListBounty 1337 4)

  liftIO $ putStrLn "===== LATEST HITS ====="
  liftIO $ print latest

  liftIO $ putStrLn "===== TOTAL BOUNTY AWARDED TO HITMAN 1 ====="
  liftIO $ print totalBountyFor1

main :: IO ()
main = withConnection connstring session
