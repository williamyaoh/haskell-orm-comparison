{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE KindSignatures #-}

module Lib where

-- Thoughts:
--
-- 1) I heavily dislike the way Squeal uses OverloadedLabels, mainly
--    because the way you do conversions to domain types, and the way
--    you do subqueries, is *extremely* verbose and requires you to
--    make sure even your column names line up.
-- 2) The combinatorial way that Squeal combines clauses and queries
--    in *theory* is something that I would like, but the way Squeal
--    implements it is frustrated and obtuse, because each clause has
--    to be nested at a different level in the query expression. Knowing
--    which level is... painful, and you have to juggle a *lot* of
--    parentheses.
-- 3) This means that you have to explicitly rename each and every column
--    of a table, even if all you're doing is selecting all the rows in
--    a single table! Because presumably the Haskell names are not the
--    same as the database names.
-- 4) This also seems to mean that there's no way to write a query that
--    returns one-off data, without defining a completely new datatype.
--    Because tuples don't have named selectors, and thus can't match up
--    with a column name in the query return. Fun.
-- 5) Squeal tries to map Haskell data to database columns by deriving Generic
--    and directly using the attribute names from the type declaration. Which
--    means that instead of giving you a way to define the correspondence once,
--    you have to state it in every single query you write. Fun.
-- 6) Squeal doesn't seem to provide a way to pass a parameter
-- .  to a subquery, so we can't abstract away certain similar queries
--    and instead have to duplicate the query code. Fun.
-- 7) I... don't think there's a way to return, say, a tuple of two different
--    entities, either. Since a tuple doesn't have named selectors. So I'd
--    need to create a completely separate datatype to hold the result
--    there. Fun.
-- 8) Overall, it feels like Squeal succeeds at... embedding SQL keywords as
--    Haskell combinators, but fails to actually be able to describe common
--    SQL patterns without breaking down.
-- 9) Sometimes using subqueries just... breaks? For some reason?

import Data.Int ( Int32, Int64 )
import Data.Scientific ( Scientific )
import Data.Text ( Text )
import Data.Time

import qualified Generics.SOP as SOP
import qualified GHC.Generics as GHC

import Squeal.PostgreSQL

-- Something that might come in handy when constructing derivative schemas
-- Compile with additional flags -XTypeFamilies -XPolyKinds
--
-- type family (++) l1 l2 where
--   (++) l1 '[] = l1
--   (++) '[] l2 = l2
--   (++) (x ': r1) l2 = x ': (r1 ++ l2)

type HandlersColumns =
  '[ "id" ::: 'Def :=> 'NotNull 'PGint4
   , "codename" ::: 'NoDef :=> 'NotNull 'PGtext
   , "created_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   , "updated_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   ]
type HandlersConstraints =
  '[ "pk_handlers" ::: 'PrimaryKey '[ "id" ]
   , "handlers_valid_updated_at" ::: 'Check '[ "updated_at", "created_at" ]
   , "handlers_unique_codenames" ::: 'Unique '[ "codename" ]
   ]

type HitmenColumns =
  '[ "id" ::: 'Def :=> 'NotNull 'PGint4
   , "codename" ::: 'NoDef :=> 'NotNull 'PGtext
   , "handler_id" ::: 'NoDef :=> 'NotNull 'PGint4
   , "created_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   , "updated_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   ]
type HitmenConstraints =
  '[ "pk_hitmen" ::: 'PrimaryKey '[ "id" ]
   , "fk_handler_id" ::: 'ForeignKey '[ "handler_id" ] "handlers" '[ "id" ]
   , "hitmen_valid_updated_at" ::: 'Check '[ "updated_at", "created_at" ]
   , "hitmen_unique_codenames" ::: 'Unique '[ "codename" ]
   ]

type MarksColumns =
  '[ "id" ::: 'Def :=> 'NotNull 'PGint4
   , "list_bounty" ::: 'NoDef :=> 'NotNull 'PGint8
   , "first_name" ::: 'NoDef :=> 'NotNull 'PGtext
   , "last_name" ::: 'NoDef :=> 'NotNull 'PGtext
   , "description" ::: 'Def :=> 'Null 'PGtext
   , "created_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   , "updated_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   ]
type MarksConstraints =
  '[ "pk_marks" ::: 'PrimaryKey '[ "id" ]
   , "marks_valid_updated_at" ::: 'Check '[ "updated_at", "created_at" ]
   ]

type PursuingMarksColumns =
  '[ "hitman_id" ::: 'NoDef :=> 'NotNull 'PGint4
   , "mark_id" ::: 'NoDef :=> 'NotNull 'PGint4
   , "created_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   , "updated_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   ]
type PursuingMarksConstraints =
  '[ "pk_pursuing_marks" ::: 'PrimaryKey '[ "hitman_id", "mark_id" ]
   , "pursuing_marks_valid_updated_at" ::: 'Check '[ "updated_at", "created_at" ]
   ]

type ErasedMarksColumns =
  '[ "hitman_id" ::: 'NoDef :=> 'NotNull 'PGint4
   , "mark_id" ::: 'NoDef :=> 'NotNull 'PGint4
   , "awarded_bounty" ::: 'NoDef :=> 'NotNull 'PGint8
   , "created_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   , "updated_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   ]
type ErasedMarksConstraints =
  '[ "pk_erased_marks" ::: 'PrimaryKey '[ "hitman_id", "mark_id" ]
   , "erased_marks_valid_updated_at" ::: 'Check '[ "updated_at", "created_at" ]
   ]

type Schema_ =
  '[ "handlers" ::: 'Table (HandlersConstraints :=> HandlersColumns)
   , "hitmen" ::: 'Table (HitmenConstraints :=> HitmenColumns)
   , "marks" ::: 'Table (MarksConstraints :=> MarksColumns)
   , "pursuing_marks" ::: 'Table (PursuingMarksConstraints :=> PursuingMarksColumns)
   , "erased_marks" ::: 'Table (ErasedMarksConstraints :=> ErasedMarksColumns)
   ]
type Schema = Public Schema_

data Handler = Handler
  { handlerID :: Int32
  , handlerCodename :: Text
  , handlerCreatedAt :: UTCTime
  , handlerUpdatedAt :: UTCTime
  }
  deriving (Show, GHC.Generic)

data Hitman = Hitman
  { hitmanID :: Int32
  , hitmanCodename :: Text
  , hitmanHandlerID :: Int32
  , hitmanCreatedAt :: UTCTime
  , hitmanUpdatedAt :: UTCTime
  }
  deriving (Show, GHC.Generic)

data Mark = Mark
  { markID :: Int32
  , markListBounty :: Int64
  , markFirstName :: Text
  , markLastName :: Text
  , markDescription :: Maybe Text
  , markCreatedAt :: UTCTime
  , markUpdatedAt :: UTCTime
  }
  deriving (Show, GHC.Generic)

data PursuingMark = PursuingMark
  { pursuingMarkHitmanID :: Int32
  , pursuingMarkMarkID :: Int32
  , pursuingMarkCreatedAt :: UTCTime
  , pursuingMarkUpdatedAt :: UTCTime
  }
  deriving (Show, GHC.Generic)

data ErasedMark = ErasedMark
  { erasedMarkHitmanID :: Int32
  , erasedMarkMarkID :: Int32
  , erasedMarkAwardedBounty :: Int64
  , erasedMarkCreatedAt :: UTCTime
  , erasedMarkUpdatedAt :: UTCTime
  }
  deriving (Show, GHC.Generic)

instance SOP.Generic Handler
instance SOP.HasDatatypeInfo Handler

instance SOP.Generic Hitman
instance SOP.HasDatatypeInfo Hitman

instance SOP.Generic Mark
instance SOP.HasDatatypeInfo Mark

instance SOP.Generic PursuingMark
instance SOP.HasDatatypeInfo PursuingMark

instance SOP.Generic ErasedMark
instance SOP.HasDatatypeInfo ErasedMark

-- I can't believe we have to define this ourselves.
timestamptz :: TypeExpression schema (nullity 'PGtimestamptz)
timestamptz = timestampWithTimeZone

timestamps :: NP (Aliased (ColumnTypeExpression tbls))
  '[ "created_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   , "updated_at" ::: 'Def :=> 'NotNull 'PGtimestamptz
   ]
timestamps =
     (timestamptz & notNullable & default_ currentTimestamp) `as` #created_at
  :* (timestamptz & notNullable & default_ currentTimestamp) `as` #updated_at

setup :: Definition (Public '[]) Schema
setup =
  createTable #handlers
    ( serial `as` #id
   :* (text & notNullable) `as` #codename
   :* timestamps
    )
    ( primaryKey #id `as` #pk_handlers
   :* check (#updated_at :* #created_at) (#updated_at .>= #created_at)
        `as` #handlers_valid_updated_at
   :* unique #codename `as` #handlers_unique_codenames
    )
  >>>
  createTable #hitmen
    ( serial `as` #id
   :* (text & notNullable) `as` #codename
   :* (int4 & notNullable) `as` #handler_id
   :* timestamps
    )
    ( primaryKey #id `as` #pk_hitmen
   :* foreignKey #handler_id #handlers #id
        OnDeleteCascade OnUpdateCascade `as` #fk_handler_id
   :* check (#updated_at :* #created_at) (#updated_at .>= #created_at)
        `as` #hitmen_valid_updated_at
   :* unique #codename `as` #hitmen_unique_codenames
    )
  >>>
  createTable #marks
    ( serial `as` #id
   :* (int8 & notNullable) `as` #list_bounty
   :* (text & notNullable) `as` #first_name
   :* (text & notNullable) `as` #last_name
   :* (text & nullable & default_ null_) `as` #description
   :* timestamps
    )
    ( primaryKey #id `as` #pk_marks
   :* check (#updated_at :* #created_at) (#updated_at .>= #created_at)
        `as` #marks_valid_updated_at
    )
  >>>
  createTable #pursuing_marks
    ( (int4 & notNullable) `as` #hitman_id
   :* (int4 & notNullable) `as` #mark_id
   :* timestamps
    )
    ( primaryKey (#hitman_id :* #mark_id) `as` #pk_pursuing_marks
   :* check (#updated_at :* #created_at) (#updated_at .>= #created_at)
        `as` #pursuing_marks_valid_updated_at
    )
  >>>
  createTable #erased_marks
    ( (int4 & notNullable) `as` #hitman_id
   :* (int4 & notNullable) `as` #mark_id
   :* (int8 & notNullable) `as` #awarded_bounty
   :* timestamps
    )
    ( primaryKey (#hitman_id :* #mark_id) `as` #pk_erased_marks
   :* check (#updated_at :* #created_at) (#updated_at .>= #created_at)
        `as` #erased_marks_valid_updated_at
    )

--------------------
-- QUERIES
--------------------

allHitmen :: Query_ Schema () Hitman
allHitmen = select_
  ( #h ! #id `as` #hitmanID
 :* #h ! #codename `as` #hitmanCodename
 :* #h ! #handler_id `as` #hitmanHandlerID
 :* #h ! #created_at `as` #hitmanCreatedAt
 :* #h ! #updated_at `as` #hitmanUpdatedAt
  )
  ( from (table (#hitmen `as` #h)) )

activeMarks :: Query_ Schema () (Only Int32)
activeMarks = select_
  ( #m ! #id `as` #fromOnly )
  ( from (table (#marks `as` #m)
    & leftOuterJoin (table (#erased_marks `as` #em))
        (#em ! #mark_id .== #m ! #id))
    & where_ (isNull $ #em ! #mark_id) )

activelyPursuingMarks :: Query_ Schema () Hitman
activelyPursuingMarks = selectDistinct_
  ( #h ! #id `as` #hitmanID
 :* #h ! #codename `as` #hitmanCodename
 :* #h ! #handler_id `as` #hitmanHandlerID
 :* #h ! #created_at `as` #hitmanCreatedAt
 :* #h ! #updated_at `as` #hitmanUpdatedAt
  )
  (( from (table (#hitmen `as` #h)
     & innerJoin (table (#pursuing_marks `as` #pm))
         (#pm ! #hitman_id .== #h ! #id)
     & innerJoin (subquery (activeMarks `as` #active))
         (#active ! #fromOnly .== #pm ! #mark_id)) ))

erasedSince :: Query_ Schema (Only UTCTime) Mark
erasedSince = select_
  ( #m ! #id `as` #markID
 :* #m ! #list_bounty `as` #markListBounty
 :* #m ! #first_name `as` #markFirstName
 :* #m ! #last_name `as` #markLastName
 :* #m ! #description `as` #markDescription
 :* #m ! #created_at `as` #markCreatedAt
 :* #m ! #updated_at `as` #markUpdatedAt
  )
  (( from (table (#marks `as` #m )
     & innerJoin (table (#erased_marks `as` #em))
        (#m ! #id .== #em ! #mark_id)))
     & where_ (#em ! #created_at .>= param @1) )

erasedSinceBy :: Query_ Schema (UTCTime, Int32) Mark
erasedSinceBy = select_
  ( #m ! #id `as` #markID
 :* #m ! #list_bounty `as` #markListBounty
 :* #m ! #first_name `as` #markFirstName
 :* #m ! #last_name `as` #markLastName
 :* #m ! #description `as` #markDescription
 :* #m ! #created_at `as` #markCreatedAt
 :* #m ! #updated_at `as` #markUpdatedAt
  )
  (( from (table (#marks `as` #m )
     & innerJoin (table (#erased_marks `as` #em))
        (#m ! #id .== #em ! #mark_id)))
     & where_ (#em ! #created_at .>= param @1)
     & where_ (#em ! #hitman_id .== param @2) )

data BountyInfo = BountyInfo
  { biHitmanID :: Int32
  , biTotalAwardedBounty :: Scientific
  }
  deriving (Show, GHC.Generic)

instance SOP.Generic BountyInfo
instance SOP.HasDatatypeInfo BountyInfo

totalBountiesAwarded :: Query_ Schema () BountyInfo
totalBountiesAwarded = select_
  ( #em ! #hitman_id `as` #biHitmanID
 :* (fromNull 0 $ sum_ (All (#em ! #awarded_bounty))) `as` #biTotalAwardedBounty
  )
  ( from (table (#erased_marks `as` #em))
    & groupBy (#em ! #hitman_id) )

-- What a joke, using `subquery` here just causes everything to blow up
-- with an inscrutable type error. So we just have to copy paste... again.
totalBountyAwarded :: Int32 -> Query_ Schema () BountyInfo
totalBountyAwarded toID = select_
  ( #em ! #hitman_id `as` #biHitmanID
 :* (fromNull 0 $ sum_ (All (#em ! #awarded_bounty))) `as` #biTotalAwardedBounty
  )
  ( from (table (#erased_marks `as` #em))
    & where_ (#em ! #hitman_id .== literal toID)
    & groupBy (#em ! #hitman_id) )

data HitInfo = HitInfo
  { hiHitmanID :: Int32
  , hiMarkID :: Int32
  }
  deriving (Show, GHC.Generic)

instance SOP.Generic HitInfo
instance SOP.HasDatatypeInfo HitInfo

latestHits :: Query_ Schema () HitInfo
latestHits = select_
  (#minid ! #hitman_id `as` #hiHitmanID :* #minid ! #mark_id `as` #hiMarkID)
  ( from ((subquery ((select_
           ( #em ! #hitman_id
          :* #em ! #created_at
          :* (fromNull (literal @Int32 (-1)) (min_ (All (#em ! #mark_id)))) `as` #mark_id
           )
           ( from (table (#erased_marks `as` #em))
             & groupBy (#em ! #hitman_id :* #em ! #created_at ))) `as` #minid ))
    & innerJoin (subquery ((select_
                  ( #em ! #hitman_id
                 :* (max_ (All (#em ! #created_at))) `as` #created_at
                  )
                  ( from (table (#erased_marks `as` #em))
                    & groupBy (#em ! #hitman_id) )) `as` #latest))
        (#minid ! #created_at .== #latest ! #created_at)) )

latestHit :: Int32 -> Query_ Schema () HitInfo
latestHit id = select Star
  ( from (subquery (latestHits `as` #latest))
    & where_ (#latest ! #hiHitmanID .== literal id) )

singularPursuer :: Query_ Schema () HitInfo
singularPursuer = select_
  ( #pm ! #hitman_id `as` #hiHitmanID :* #pm ! #mark_id `as` #hiMarkID )
  ( from (subquery ((select_
           ( #pm ! #mark_id )
           ( from (table (#pursuing_marks `as` #pm))
             & groupBy (#pm ! #mark_id)
             & having (count (All (#pm ! #hitman_id)) .== literal @Int64 1) ))
           `as` #counts)
    & innerJoin (subquery (activeMarks `as` #active))
        (#active ! #fromOnly .== #counts ! #mark_id)
    & innerJoin (table (#pursuing_marks `as` #pm))
        (#pm ! #mark_id .== #counts ! #mark_id)) )

marksOfOpportunity :: Query_ Schema () HitInfo
marksOfOpportunity = select_
  (#em ! #hitman_id `as` #hiHitmanID :* #em ! #mark_id `as` #hiMarkID)
  ( from (table (#erased_marks `as` #em)
    & leftOuterJoin (table (#pursuing_marks `as` #pm))
        (#em ! #hitman_id .== #pm ! #hitman_id .&&
         #em ! #mark_id .== #pm ! #mark_id))
    & where_ (isNull (#pm ! #hitman_id)) )

--------------------
-- INSERTS/UPDATES
--------------------

insertHandler :: Manipulation_ Schema Handler (Only Int32)
insertHandler = insertInto #handlers
  (Values_ ( Default `as` #id
          :* Set (param @2) `as` #codename
          :* Default `as` #created_at
          :* Default `as` #updated_at
           ))
  OnConflictDoRaise
  (Returning_ (#id `as` #fromOnly))

insertHitman :: Manipulation_ Schema Hitman (Only Int32)
insertHitman = insertInto #hitmen
  (Values_ ( Default `as` #id
          :* Set (param @2) `as` #codename
          :* Set (param @3) `as` #handler_id
          :* Default `as` #created_at
          :* Default `as` #updated_at
           ))
  OnConflictDoRaise
  (Returning_ (#id `as` #fromOnly))

insertMark :: Manipulation_ Schema Mark (Only Int32)
insertMark = insertInto #marks
  -- lol
  (Values_ ( Default `as` #id
          :* Set (param @2) `as` #list_bounty
          :* Set (param @3) `as` #first_name
          :* Set (param @4) `as` #last_name
          :* Set (param @5) `as` #description
          :* Default `as` #created_at
          :* Default `as` #updated_at
           ))
  OnConflictDoRaise
  (Returning_ (#id `as` #fromOnly))

insertPursuingMark :: Manipulation_ Schema PursuingMark ()
insertPursuingMark = insertInto #pursuing_marks
  (Values_ ( Set (param @1) `as` #hitman_id
          :* Set (param @2) `as` #mark_id
          :* Default `as` #created_at
          :* Default `as` #updated_at
           ))
  OnConflictDoRaise
  (Returning_ Nil)

insertErasedMark :: Manipulation_ Schema ErasedMark ()
insertErasedMark = insertInto #erased_marks
  (Values_ ( Set (param @1) `as` #hitman_id
          :* Set (param @2) `as` #mark_id
          :* Set (param @3) `as` #awarded_bounty
          :* Default `as` #created_at
          :* Default `as` #updated_at
           ))
  OnConflictDoRaise
  (Returning_ Nil)

increaseListBounty :: Int64 -> Int32 -> Manipulation_ Schema () ()
increaseListBounty increase mark_id = update #marks
  ( Set (#list_bounty + literal increase) `as` #list_bounty
 :* Set currentTimestamp `as` #updated_at
  )
  (#id .== literal mark_id)
  (Returning_ Nil)
