{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Lib where

-- Thoughts:
--
-- 1) Beam wants to be completely backend-agnostic, but as a result the
--    typeclasses become ridiculously verbose.
-- 2) The types of the queries are awful. Just a mess of QExpr, QAgg everywhere,
--    an incomprehensible scoping parameter, having to specify which backend the
--    query is targeting... it's just a mess.
-- 3) That said, it's actually very easy to compose subqueries and type queries
--    to return whatever rows and combinations of data that you want. That's good!
-- 4) There's no easy way to get distincts? So you just have to aggregate and do
--    a groupBy.
-- 5) Unfortunately, Beam has the problem with sums changing the result types,
--    but Beam's aggregation functions don't correctly reflect this. So unsafe
--    casts are required.
-- 6) Overall, Beam is actually pretty usable for writing queries, but the types
--    just make it obnoxious to work with.

import Data.Text
import Data.Fixed
import Data.Int
import Data.Time
import Data.Scientific

import Database.Beam
import Database.Beam.Postgres

mkUTCTime :: (Integer, Int, Int)
          -> (Int, Int, Pico)
          -> UTCTime
mkUTCTime (year, mon, day) (hour, min, sec) =
  UTCTime (fromGregorian year mon day)
          (timeOfDayToTime (TimeOfDay hour min sec))

data HandlerT f = Handler
  { _handlerID :: C f Int32
  , _handlerCodename :: C f Text
  , _handlerCreatedAt :: C f UTCTime
  , _handlerUpdatedAt :: C f UTCTime
  }
  deriving (Generic, Beamable)

data HitmanT f = Hitman
  { _hitmanID :: C f Int32
  , _hitmanCodename :: C f Text
  , _hitmanHandler :: PrimaryKey HandlerT f
  , _hitmanCreatedAt :: C f UTCTime
  , _hitmanUpdatedAt :: C f UTCTime
  }
  deriving (Generic, Beamable)

data MarkT f = Mark
  { _markID :: C f Int32
  , _markListBounty :: C f Int64
  , _markFirstName :: C f Text
  , _markLastName :: C f Text
  , _markDescription :: C f (Maybe Text)
  , _markCreatedAt :: C f UTCTime
  , _markUpdatedAt :: C f UTCTime
  }
  deriving (Generic, Beamable)

data PursuingMarkT f = PursuingMark
  { _pursuingMarkHitman :: PrimaryKey HitmanT f
  , _pursuingMarkMark :: PrimaryKey MarkT f
  , _pursuingMarkCreatedAt :: C f UTCTime
  , _pursuingMarkUpdatedAt :: C f UTCTime
  }
  deriving (Generic, Beamable)

data ErasedMarkT f = ErasedMark
  { _erasedMarkHitman :: PrimaryKey HitmanT f
  , _erasedMarkMark :: PrimaryKey MarkT f
  , _erasedMarkAwardedBounty :: C f Int64
  , _erasedMarkCreatedAt :: C f UTCTime
  , _erasedMarkUpdatedAt :: C f UTCTime
  }
  deriving (Generic, Beamable)

data HitmanDB f = HitmanDB
  { _dbHandlers :: f (TableEntity HandlerT)
  , _dbHitmen :: f (TableEntity HitmanT)
  , _dbMarks :: f (TableEntity MarkT)
  , _dbPursuingMarks :: f (TableEntity PursuingMarkT)
  , _dbErasedMarks :: f (TableEntity ErasedMarkT)
  }
  deriving (Generic, Database be)

type Handler = HandlerT Identity
type Hitman = HitmanT Identity
type Mark = MarkT Identity
type PursuingMark = PursuingMarkT Identity
type ErasedMark = ErasedMarkT Identity

instance Table HandlerT where
  data PrimaryKey HandlerT f = HandlerID (C f Int32)
    deriving (Generic, Beamable)
  primaryKey = HandlerID . _handlerID

instance Table HitmanT where
  data PrimaryKey HitmanT f = HitmanID (C f Int32)
    deriving (Generic, Beamable)
  primaryKey = HitmanID . _hitmanID

instance Table MarkT where
  data PrimaryKey MarkT f = MarkID (C f Int32)
    deriving (Generic, Beamable)
  primaryKey = MarkID . _markID

instance Table PursuingMarkT where
  data PrimaryKey PursuingMarkT f = PursuingMarkID (PrimaryKey HitmanT f) (PrimaryKey MarkT f)
    deriving (Generic, Beamable)
  primaryKey = PursuingMarkID <$> _pursuingMarkHitman <*> _pursuingMarkMark

instance Table ErasedMarkT where
  data PrimaryKey ErasedMarkT f = ErasedMarkID (PrimaryKey HitmanT f) (PrimaryKey MarkT f)
    deriving (Generic, Beamable)
  primaryKey = ErasedMarkID <$> _erasedMarkHitman <*> _erasedMarkMark

deriving instance Show Handler
deriving instance Show Hitman
deriving instance Show Mark
deriving instance Show PursuingMark
deriving instance Show ErasedMark

deriving instance Show (PrimaryKey HandlerT Identity)
deriving instance Show (PrimaryKey HitmanT Identity)
deriving instance Show (PrimaryKey MarkT Identity)
deriving instance Show (PrimaryKey PursuingMarkT Identity)
deriving instance Show (PrimaryKey ErasedMarkT Identity)

hitmanDB :: DatabaseSettings Postgres HitmanDB
hitmanDB = defaultDbSettings `withDbModification`
  dbModification
    { _dbHandlers = setEntityName "handlers" <>
      modifyTableFields tableModification
        { _handlerID = fieldNamed "id" }
    , _dbHitmen = setEntityName "hitmen" <>
      modifyTableFields tableModification
        { _hitmanID = fieldNamed "id"
        , _hitmanHandler = HandlerID (fieldNamed "handler_id")
        }
    , _dbMarks = setEntityName "marks" <>
      modifyTableFields tableModification
        { _markID = fieldNamed "id" }
    , _dbPursuingMarks = setEntityName "pursuing_marks" <>
      modifyTableFields tableModification
        { _pursuingMarkHitman = HitmanID (fieldNamed "hitman_id")
        , _pursuingMarkMark = MarkID (fieldNamed "mark_id")
        , _pursuingMarkCreatedAt = fieldNamed "created_at"
        , _pursuingMarkUpdatedAt = fieldNamed "updated_at"
        }
    , _dbErasedMarks = setEntityName "erased_marks" <>
      modifyTableFields tableModification
        { _erasedMarkHitman = HitmanID (fieldNamed "hitman_id")
        , _erasedMarkMark = MarkID (fieldNamed "mark_id")
        , _erasedMarkAwardedBounty = fieldNamed "awarded_bounty"
        , _erasedMarkCreatedAt = fieldNamed "created_at"
        , _erasedMarkUpdatedAt = fieldNamed "updated_at"
        }
    }

--------------------
-- QUERIES
--------------------

allHandlers :: Q Postgres HitmanDB s (HandlerT (QExpr Postgres s))
allHandlers = all_ (_dbHandlers hitmanDB)

allHitmen :: Q Postgres HitmanDB s (HitmanT (QExpr Postgres s))
allHitmen = all_ (_dbHitmen hitmanDB)

allMarks :: Q Postgres HitmanDB s (MarkT (QExpr Postgres s))
allMarks = all_ (_dbMarks hitmanDB)

allPursuingMarks :: Q Postgres HitmanDB s (PursuingMarkT (QExpr Postgres s))
allPursuingMarks = all_ (_dbPursuingMarks hitmanDB)

allErasedMarks :: Q Postgres HitmanDB s (ErasedMarkT (QExpr Postgres s))
allErasedMarks = all_ (_dbErasedMarks hitmanDB)

activelyPursuingMarks :: Q Postgres HitmanDB s (HitmanT (QExpr Postgres s))
activelyPursuingMarks = do
  hID <- aggregate_ (\h -> group_ (_hitmanID h)) (do
           pursuingMarks <- allPursuingMarks
           erasedMarks <- leftJoin_ allErasedMarks (\em -> _erasedMarkMark em ==. _pursuingMarkMark pursuingMarks)
           hitmen <- related_ (_dbHitmen hitmanDB) (_pursuingMarkHitman pursuingMarks)

           guard_ (isNothing_ (_erasedMarkHitman erasedMarks))

           pure hitmen)
  hitmen <- related_ (_dbHitmen hitmanDB) (HitmanID hID)
  pure hitmen

erasedSince :: UTCTime -> Q Postgres HitmanDB s (HitmanT (QExpr Postgres s), MarkT (QExpr Postgres s))
erasedSince since = do
  erasedMarks <- allErasedMarks
  marks <- related_ (_dbMarks hitmanDB) (_erasedMarkMark erasedMarks)
  hitmen <- related_ (_dbHitmen hitmanDB) (_erasedMarkHitman erasedMarks)

  guard_ (_erasedMarkCreatedAt erasedMarks >=. val_ since)

  pure (hitmen, marks)

erasedSinceBy :: UTCTime
              -> PrimaryKey HitmanT Identity
              -> Q Postgres HitmanDB s (MarkT (QExpr Postgres s))
erasedSinceBy since (HitmanID id) = do
  (hitmen, marks) <- erasedSince since

  guard_ (_hitmanID hitmen ==. val_ id)

  pure marks

totalBountiesAwarded :: Q Postgres HitmanDB s
  ( HitmanT (QExpr Postgres s)
  , QExpr Postgres s Scientific
  )
totalBountiesAwarded = do
  (hID, total) <- aggregate_
    (\em -> ( group_ (_erasedMarkHitman em)
            -- This cast is unfortunately necessary; the query will typecheck
            -- but fail at runtime if we don't add it.
            , cast_ (fromMaybe_ 0 (sum_ (_erasedMarkAwardedBounty em))) (numeric Nothing)
            ))
    allErasedMarks
  hitmen <- related_ (_dbHitmen hitmanDB) hID
  pure (hitmen, total)

totalBountyAwarded :: PrimaryKey HitmanT Identity -> Q Postgres HitmanDB s (QExpr Postgres s Scientific)
totalBountyAwarded (HitmanID to) = do
  (hitmen, bounty) <- totalBountiesAwarded

  guard_ (_hitmanID hitmen ==. val_ to)

  pure bounty

latestHits :: Q Postgres HitmanDB s
  ( HitmanT (QExpr Postgres s)
  , MarkT (QExpr Postgres s)
  )
latestHits = do
  hitman <- allHitmen
  mark <- allMarks

  (minHID, minCreated, minMarkID) <- minID
  (latestHID, latestCreated) <- latest

  guard_ (minHID ==. latestHID)
  guard_ (just_ minCreated ==. latestCreated)
  guard_ (minHID ==. HitmanID (_hitmanID hitman))
  guard_ (minMarkID ==. just_ (_markID mark))

  pure (hitman, mark)

  where minID = aggregate_ (\em -> ( group_ (_erasedMarkHitman em)
                                   , group_ (_erasedMarkCreatedAt em)
                                   , min_ (getMarkID $ _erasedMarkMark em)
                                   ))
                           allErasedMarks

        latest = aggregate_ (\em -> ( group_ (_erasedMarkHitman em)
                                    , max_ (_erasedMarkCreatedAt em)
                                    ))
                            allErasedMarks

        getMarkID (MarkID id) = id

latestHit :: PrimaryKey HitmanT Identity -> Q Postgres HitmanDB s (MarkT (QExpr Postgres s))
latestHit (HitmanID id) = do
  (hitman, mark) <- latestHits

  guard_ (_hitmanID hitman ==. val_ id)

  pure mark

singularPursuer :: Q Postgres HitmanDB s (HitmanT (QExpr Postgres s), MarkT (QExpr Postgres s))
singularPursuer = do
  markID <- withOnlyOne
  erasedMark <- leftJoin_ allErasedMarks (\em -> _erasedMarkMark em ==. MarkID markID)
  pursuing <- allPursuingMarks
  marks <- related_ (_dbMarks hitmanDB) (MarkID markID)
  hitmen <- related_ (_dbHitmen hitmanDB) (_pursuingMarkHitman pursuing)

  guard_ (_pursuingMarkMark pursuing ==. MarkID (_markID marks))
  guard_ (isNothing_ (_erasedMarkHitman erasedMark))

  pure (hitmen, marks)

  where withPursuing = do
          marks <- allMarks
          pursuingMarks <- allPursuingMarks

          guard_ (MarkID (_markID marks) ==. _pursuingMarkMark pursuingMarks)

          pure (marks, pursuingMarks)

        pursuingCount = aggregate_ (\(m, pm) -> ( group_ (_markID m)
                                                , as_ @Int64 (count_ (getHitmanID $ _pursuingMarkHitman pm))
                                                ))
                                   withPursuing

        withOnlyOne = do
          (markID, pursuers) <- pursuingCount

          guard_ (pursuers ==. 1)

          pure markID

        getHitmanID (HitmanID id) = id

marksOfOpportunity :: Q Postgres HitmanDB s (HitmanT (QExpr Postgres s), MarkT (QExpr Postgres s))
marksOfOpportunity = do
  erasedMarks <- allErasedMarks
  pursuingMarks <- leftJoin_ allPursuingMarks (\pm -> _pursuingMarkMark pm ==. _erasedMarkMark erasedMarks)
  marks <- related_ (_dbMarks hitmanDB) (_erasedMarkMark erasedMarks)
  hitmen <- related_ (_dbHitmen hitmanDB) (_erasedMarkHitman erasedMarks)

  guard_ (isNothing_ (_pursuingMarkHitman pursuingMarks))

  pure (hitmen, marks)

--------------------
-- INSERTS/UPDATES
--------------------

insertSeedHandlers :: Connection -> IO ()
insertSeedHandlers conn = runBeamPostgresDebug putStrLn conn $ runInsert $
  insert (_dbHandlers hitmanDB) $
    insertExpressions
      [ Handler default_ (val_ "Olive") default_ default_
      , Handler default_ (val_ "Pallas") default_ default_
      ]

insertSeedHitmen :: Connection -> IO ()
insertSeedHitmen conn = runBeamPostgresDebug putStrLn conn $ runInsert $
  insert (_dbHitmen hitmanDB) $
    insertExpressions
      [ Hitman default_ (val_ "Callaird") (HandlerID $ val_ 1) default_ default_
      , Hitman default_ (val_ "Bomois") (HandlerID $ val_ 1) default_ default_
      , Hitman default_ (val_ "Dune") (HandlerID $ val_ 2) default_ default_
      ]

insertSeedMarks :: Connection -> IO ()
insertSeedMarks conn = runBeamPostgresDebug putStrLn conn $ runInsert $
  insert (_dbMarks hitmanDB) $
    insertExpressions
      [ Mark default_ (val_ 25000) (val_ "John") (val_ "Tosti") (val_ Nothing) default_ default_
      , Mark default_ (val_ 50000) (val_ "Macie") (val_ "Jordan") (val_ Nothing) default_ default_
      , Mark default_ (val_ 33000) (val_ "Sal") (val_ "Aspot") (val_ Nothing) default_ default_
      , Mark default_ (val_ 10000) (val_ "Lars") (val_ "Andersen") (val_ Nothing) default_ default_
      ]

insertSeedPursuingMarks :: Connection -> IO ()
insertSeedPursuingMarks conn = runBeamPostgresDebug putStrLn conn $ runInsert $
  insert (_dbPursuingMarks hitmanDB) $
    insertExpressions
      [ PursuingMark (HitmanID $ val_ 1) (MarkID $ val_ 2) (val_ $ mkUTCTime (2018, 07, 01) (0, 0, 0)) default_
      , PursuingMark (HitmanID $ val_ 2) (MarkID $ val_ 2) (val_ $ mkUTCTime (2018, 07, 02) (0, 0, 0)) default_
      , PursuingMark (HitmanID $ val_ 2) (MarkID $ val_ 4) (val_ $ mkUTCTime (2019, 05, 05) (0, 0, 0)) default_
      , PursuingMark (HitmanID $ val_ 3) (MarkID $ val_ 3) (val_ $ mkUTCTime (2018, 05, 13) (0, 0, 0)) default_
      , PursuingMark (HitmanID $ val_ 3) (MarkID $ val_ 2) (val_ $ mkUTCTime (2019, 02, 15) (0, 0, 0)) default_
      ]

insertSeedErasedMarks :: Connection -> IO ()
insertSeedErasedMarks conn = runBeamPostgresDebug putStrLn conn $ runInsert $
    insert (_dbErasedMarks hitmanDB) $
      insertExpressions
        [ ErasedMark (HitmanID $ val_ 1) (MarkID $ val_ 2) (val_ 30000) (val_ $ mkUTCTime (2018, 09, 03) (0, 0, 0)) default_
        , ErasedMark (HitmanID $ val_ 1) (MarkID $ val_ 1) (val_ 55000) (val_ $ mkUTCTime (2019, 02, 02) (0, 0, 0)) default_
        , ErasedMark (HitmanID $ val_ 3) (MarkID $ val_ 3) (val_ 27000) (val_ $ mkUTCTime (2018, 06, 30) (0, 0, 0)) default_
        ]

increaseListBounty :: Int64
                   -> PrimaryKey MarkT Identity
                   -> SqlUpdate Postgres MarkT
increaseListBounty amt (MarkID id) = updateTable (_dbMarks hitmanDB)
  (set { _markListBounty = toUpdatedValue (\m -> _markListBounty m + val_ amt) })
  (\m -> _markID m ==. val_ id)
