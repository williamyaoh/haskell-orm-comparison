{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Lib where

-- Thoughts:
--
-- 1) Using product-profunctors for table definitions definitely ups
--    the amount of upfront cost needed.
-- 2) That said, this means that it's fairly easy to extend tables with
--    common columns; representing the table definitions at runtime
--    leads to... making abstraction *possible*. Maybe not easy.
-- 3) Through the same causes, it also makes it easy to define separate
--    views into the same table if necessary, and query in different
--    ways in different places.
-- 4) The choice between `optional` and `readOnly` for table fields is a
--    little subtle. For instance, making an autoincrementing ID field
--    `readOnly` is a bad idea, because then updates will attempt to
--    use DEFAULT as the update value every time. Not what you want.
--    Instead, use `optional` for those. I recommend using `optional` as
--    your default for fields you don't want to include and only use
--    `readOnly` if you know it's safe e.g. an update timestamp.
-- 5) Sadly, no way to explicitly run queries that only expect a single result.
--    Have to pattern match on the resulting list yourself.
-- 6) For the timestamps, one thing you could do is to intentionally create a
--    "wrapper" type for common fields, and have all the "extras" be a part of
--    that. That way, when querying, it's possible to ignore all the extra
--    metadata if you don't want it.

import Prelude hiding ( sum, min, max, null )

import Data.Text hiding ( groupBy, null )
import Data.Fixed
import Data.Int
import Data.Time
import Data.Function ( (&) )

import Data.Profunctor
import Data.Profunctor.Product
import Data.Profunctor.Product.TH ( makeAdaptorAndInstanceInferrable )

import Control.Arrow

import Opaleye
import qualified Opaleye.Join

mkUTCTime :: (Integer, Int, Int)
          -> (Int, Int, Pico)
          -> UTCTime
mkUTCTime (year, mon, day) (hour, min, sec) =
  UTCTime (fromGregorian year mon day)
          (timeOfDayToTime (TimeOfDay hour min sec))

timestamps :: TableFields
  (Maybe (F SqlTimestamptz), ())
  (F SqlTimestamptz, F SqlTimestamptz)
timestamps =
  p2 (optional "created_at", readOnly "updated_at")

-- |
-- Strip the timestamp information from a table to create a new
-- queryable table for convenience purposes.
withNoMeta :: Table (a, (Maybe (F SqlTimestamptz), ())) (b, c) -> Table a b
withNoMeta tbl = tbl
  & lmap (\t -> (t, (Nothing, ())))
  & rmap fst

type F field = Field field
type FNull field = FieldNullable field

newtype HandlerID = HandlerID Int32
  deriving (Show)
newtype HitmanID = HitmanID Int32
  deriving (Show)
newtype MarkID = MarkID Int32
  deriving (Show)

-- Showing that we can use our own custom IDs when defining our types.
instance QueryRunnerColumnDefault SqlInt4 HandlerID where
  defaultFromField = HandlerID <$> defaultFromField
instance QueryRunnerColumnDefault SqlInt4 HitmanID where
  defaultFromField = HitmanID <$> defaultFromField
instance QueryRunnerColumnDefault SqlInt4 MarkID where
  defaultFromField = MarkID <$> defaultFromField

--------------------
-- HANDLER
--------------------

data HandlerT a b = Handler
  { handlerID :: a
  , handlerCodename :: b
  }
type Handler = HandlerT HandlerID Text
type HandlerWrite = HandlerT (Maybe (F SqlInt4)) (F SqlText)
type HandlerF = HandlerT (F SqlInt4) (F SqlText)

deriving instance Show Handler

$(makeAdaptorAndInstanceInferrable "pHandler" ''HandlerT)

handlerTable :: Table
  (HandlerWrite, (Maybe (F SqlTimestamptz), ()))
  (HandlerF, (F SqlTimestamptz, F SqlTimestamptz))
handlerTable = table "handlers" $
  (p2 ( pHandler Handler
          { handlerID = optional "id"
          , handlerCodename = required "codename"
          }
      , timestamps
      ))

-- |
-- Convenience table that ignores timestamps, for simpler queries.
handlersNoMeta :: Table HandlerWrite HandlerF
handlersNoMeta = withNoMeta handlerTable

--------------------
-- HITMAN
--------------------

data HitmanT a b c = Hitman
  { hitmanID :: a
  , hitmanCodename :: b
  , hitmanHandlerID :: c
  } deriving Show
type Hitman = HitmanT HitmanID Text HandlerID
type HitmanWrite = HitmanT (Maybe (F SqlInt4)) (F SqlText) (F SqlInt4)
type HitmanF = HitmanT (F SqlInt4) (F SqlText) (F SqlInt4)

$(makeAdaptorAndInstanceInferrable "pHitman" ''HitmanT)

hitmanTable :: Table
  (HitmanWrite, (Maybe (F SqlTimestamptz), ()))
  (HitmanF, (F SqlTimestamptz, F SqlTimestamptz))
hitmanTable = table "hitmen" $
  p2 ( pHitman $ Hitman
         { hitmanID = optional "id"
         , hitmanCodename = required "codename"
         , hitmanHandlerID = required "handler_id"
         }
     , timestamps
     )

-- |
-- Convenience table that ignores timestamps, for simpler queries.
hitmenNoMeta :: Table HitmanWrite HitmanF
hitmenNoMeta = withNoMeta hitmanTable

--------------------
-- MARK
--------------------

data MarkT a b c d e = Mark
  { markID :: a
  , markListBounty :: b
  , markFirstName :: c
  , markLastName :: d
  , markDescription :: e
  } deriving Show
type Mark = MarkT MarkID Int64 Text Text (Maybe Text)
type MarkWrite = MarkT (Maybe (F SqlInt4)) (F SqlInt8) (F SqlText) (F SqlText) (FieldNullable SqlText)
type MarkF = MarkT (F SqlInt4) (F SqlInt8) (F SqlText) (F SqlText) (FieldNullable SqlText)

$(makeAdaptorAndInstanceInferrable "pMark" ''MarkT)

markTable :: Table
  (MarkWrite, (Maybe (F SqlTimestamptz), ()))
  (MarkF, (F SqlTimestamptz, F SqlTimestamptz))
markTable = table "marks" $
  p2 ( pMark Mark
         { markID = optional "id"
         , markListBounty = required "list_bounty"
         , markFirstName = required "first_name"
         , markLastName = required "last_name"
         , markDescription = tableField "description"
         }
     , timestamps
     )

-- |
-- Convenience table that ignores timestamps, for simpler queries.
marksNoMeta :: Table MarkWrite MarkF
marksNoMeta = withNoMeta markTable

--------------------
-- PURSUING MARK
--------------------

data PursuingMarkT a b = PursuingMark
  { pursuingMarkHitmanID :: a
  , pursuingMarkMarkID :: b
  }
type PursuingMark = PursuingMarkT HitmanID MarkID
type PursuingMarkF = PursuingMarkT (F SqlInt4) (F SqlInt4)

deriving instance Show PursuingMark

$(makeAdaptorAndInstanceInferrable "pPursuingMark" ''PursuingMarkT)

pursuingMarkTable :: Table
  (PursuingMarkF, (Maybe (F SqlTimestamptz), ()))
  (PursuingMarkF, (F SqlTimestamptz, F SqlTimestamptz))
pursuingMarkTable = table "pursuing_marks" $
  p2 ( pPursuingMark PursuingMark
         { pursuingMarkHitmanID = required "hitman_id"
         , pursuingMarkMarkID = required "mark_id"
         }
     , timestamps
     )

-- |
-- Convenience table that ignores timestamps, for simpler queries.
pMarksNoMeta :: Table PursuingMarkF PursuingMarkF
pMarksNoMeta = withNoMeta pursuingMarkTable

--------------------
-- ERASED MARK
--------------------

data ErasedMarkT a b c = ErasedMark
  { erasedMarkHitmanID :: a
  , erasedMarkMarkID :: b
  , erasedMarkAwardedBounty :: c
  }
type ErasedMark = ErasedMarkT HitmanID MarkID Int64
type ErasedMarkF = ErasedMarkT (F SqlInt4) (F SqlInt4) (F SqlInt8)

deriving instance Show ErasedMark

$(makeAdaptorAndInstanceInferrable "pErasedMark" ''ErasedMarkT)

erasedMarkTable :: Table
  (ErasedMarkF, (Maybe (F SqlTimestamptz), ()))
  (ErasedMarkF, (F SqlTimestamptz, F SqlTimestamptz))
erasedMarkTable = table "erased_marks" $
  p2 ( pErasedMark ErasedMark
         { erasedMarkHitmanID = required "hitman_id"
         , erasedMarkMarkID = required "mark_id"
         , erasedMarkAwardedBounty = required "awarded_bounty"
         }
     , timestamps
     )

-- |
-- Convenience table that ignores timestamps, for simpler queries.
eMarksNoMeta :: Table ErasedMarkF ErasedMarkF
eMarksNoMeta = withNoMeta erasedMarkTable

--------------------
-- QUERIES
--------------------

isNothingFields :: MaybeFields a -> Field SqlBool
isNothingFields = maybeFields (sqlBool True) (\_ -> sqlBool False)

allHitmen :: Select HitmanF
allHitmen = selectTable hitmenNoMeta

activeMarks :: Select MarkF
activeMarks = do
  m <- selectTable marksNoMeta
  em <- Opaleye.Join.optional $ do
    em <- selectTable eMarksNoMeta
    viaLateral restrict $ markID m .== erasedMarkMarkID em
    pure em

  viaLateral restrict $ isNothingFields em

  pure m

activelyPursuingMarks :: Select HitmanF
activelyPursuingMarks = do
  h <- selectTable (fmap fst hitmanTable)
  pm <- selectTable (fmap fst pursuingMarkTable)
  active <- activeMarks

  viaLateral restrict $ markID active .== pursuingMarkMarkID pm
  viaLateral restrict $ hitmanID h .== pursuingMarkHitmanID pm

  pure h

erasedSince :: UTCTime -> Select MarkF
erasedSince since = do
  m <- selectTable marksNoMeta
  (ErasedMark { erasedMarkMarkID = eMarkID }, (createdAt, _))
    <- selectTable erasedMarkTable

  viaLateral restrict $ createdAt .>= sqlUTCTime since
  viaLateral restrict $ markID m .== eMarkID

  pure m

erasedSinceBy :: (UTCTime, HitmanID) -> Select MarkF
erasedSinceBy (since, (HitmanID by)) = do
  erased <- erasedSince since
  ErasedMark { erasedMarkHitmanID = hitmanID, erasedMarkMarkID = eMarkID }
    <- selectTable eMarksNoMeta

  viaLateral restrict $ markID erased .== eMarkID .&& hitmanID .== sqlInt4 (fromIntegral by)

  pure erased

-- |
-- Makes sure to return total bounty of zero even for hitmen who
-- don't have any erased marks.
totalBountiesAwarded :: Select (HitmanF, F SqlNumeric)
totalBountiesAwarded = do
  h <- selectTable hitmenNoMeta
  mTotal <- Opaleye.Join.optional $ do
    (hID, total) <- totals
    viaLateral restrict $ hitmanID h .== hID
    pure total

  pure (h, fromMaybeFields (sqlNumeric 0) mTotal)

  where totals :: Select (F SqlInt4, F SqlNumeric)
        totals =
          aggregate (p2 (groupBy, sumInt8)) $ do
            ErasedMark { erasedMarkHitmanID = hitmanID
                       , erasedMarkAwardedBounty = awarded
                       } <- selectTable eMarksNoMeta
            pure (hitmanID, awarded)

totalBountyAwarded :: HitmanID -> Select (F SqlNumeric)
totalBountyAwarded (HitmanID to) = do
  (h, total) <- totalBountiesAwarded

  viaLateral restrict $ hitmanID h .== sqlInt4 (fromIntegral to)

  pure total

latestHits :: Select (HitmanF, MarkF)
latestHits = do
  (hID, created, mID) <- byDate
  (maxHID, maxCreated) <- maxDates
  h <- selectTable hitmenNoMeta
  m <- selectTable marksNoMeta

  viaLateral restrict $ hID .== maxHID .&& created .== maxCreated
  viaLateral restrict $ hID .== hitmanID h
  viaLateral restrict $ mID .== markID m

  pure (h, m)

  where byDate = aggregate (p3 (groupBy, groupBy, min)) $ do
          ( ErasedMark { erasedMarkHitmanID = hitmanID
                       , erasedMarkMarkID = markID
                       }
            , (createdAt, _)
            ) <- selectTable erasedMarkTable
          pure (hitmanID, createdAt, markID)
        maxDates = aggregate (p2 (groupBy, max)) $ do
          ( ErasedMark { erasedMarkHitmanID = hitmanID }
            , (createdAt, _)
            ) <- selectTable erasedMarkTable
          pure (hitmanID, createdAt)

latestHit :: HitmanID -> Select MarkF
latestHit (HitmanID hitBy) = do
  (h, m) <- latestHits
  viaLateral restrict $ hitmanID h .== sqlInt4 (fromIntegral hitBy)
  pure m

-- |
-- We exclude any marks that have already been erased.
singularPursuer :: Select (HitmanF, MarkF)
singularPursuer = do
  h <- selectTable hitmenNoMeta
  m <- activeMarks
  (mID, numPursuers) <- pursuerCounts
  PursuingMark { pursuingMarkHitmanID = pHID, pursuingMarkMarkID = pMID }
    <- fmap fst (selectTable pursuingMarkTable)

  viaLateral restrict $ numPursuers .== 1
  viaLateral restrict $ mID .== pMID
  viaLateral restrict $ mID .== markID m
  viaLateral restrict $ pHID .== hitmanID h

  pure (h, m)

  where pursuerCounts =
          aggregate (p2 (groupBy, countStar)) $ do
            PursuingMark { pursuingMarkHitmanID = hitmanID
                         , pursuingMarkMarkID = markID
                         } <- fmap fst (selectTable pursuingMarkTable)
            pure (markID, hitmanID)

marksOfOpportunity :: Select (HitmanF, MarkF)
marksOfOpportunity = do
  h <- selectTable hitmenNoMeta
  m <- selectTable marksNoMeta
  ErasedMark { erasedMarkHitmanID = eHID, erasedMarkMarkID = eMID }
    <- fmap fst (selectTable erasedMarkTable)
  pHID <- Opaleye.Join.optional $ do
    PursuingMark { pursuingMarkHitmanID = pHitmanID, pursuingMarkMarkID = pMarkID }
      <- fmap fst (selectTable pursuingMarkTable)
    viaLateral restrict $ eHID .== pHitmanID .&& eMID .== pMarkID

  viaLateral restrict $ isNothingFields pHID
  viaLateral restrict $ hitmanID h .== eHID
  viaLateral restrict $ markID m .== eMID

  pure (h, m)

--------------------
-- INSERTS/UPDATES
--------------------

insertSeedHandlers :: Insert Int64
insertSeedHandlers = Insert
  { iTable = handlerTable
  , iRows =
    [ (Handler Nothing (sqlString "Olive"), (Nothing, ()))
    , (Handler Nothing (sqlString "Pallas"), (Nothing, ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

insertSeedHitmen :: Insert Int64
insertSeedHitmen = Insert
  { iTable = hitmanTable
  , iRows =
    [ (Hitman Nothing (sqlString "Callaird") 1, (Nothing, ()))
    , (Hitman Nothing (sqlString "Bomois") 1, (Nothing, ()))
    , (Hitman Nothing (sqlString "Dune") 2, (Nothing, ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

insertSeedMarks :: Insert Int64
insertSeedMarks = Insert
  { iTable = markTable
  , iRows =
    [ (Mark Nothing 25000 (sqlString "John") (sqlString "Tosti") null, (Nothing, ()))
    , (Mark Nothing 50000 (sqlString "Macie") (sqlString "Jordan") null, (Nothing, ()))
    , (Mark Nothing 33000 (sqlString "Sal") (sqlString "Aspot") null, (Nothing, ()))
    , (Mark Nothing 10000 (sqlString "Lars") (sqlString "Andersen") null, (Nothing, ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

insertSeedPursuingMarks :: Insert Int64
insertSeedPursuingMarks = Insert
  { iTable = pursuingMarkTable
  , iRows =
    [ (PursuingMark 1 2, (Just $ sqlUTCTime (mkUTCTime (2018, 07, 01) (0, 0, 0)), ()))
    , (PursuingMark 2 2, (Just $ sqlUTCTime (mkUTCTime (2018, 07, 02) (0, 0, 0)), ()))
    , (PursuingMark 2 4, (Just $ sqlUTCTime (mkUTCTime (2019, 05, 05) (0, 0, 0)), ()))
    , (PursuingMark 3 3, (Just $ sqlUTCTime (mkUTCTime (2018, 05, 13) (0, 0, 0)), ()))
    , (PursuingMark 3 2, (Just $ sqlUTCTime (mkUTCTime (2019, 02, 15) (0, 0, 0)), ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

insertSeedErasedMarks :: Insert Int64
insertSeedErasedMarks = Insert
  { iTable = erasedMarkTable
  , iRows =
    [ (ErasedMark 1 2 30000, (Just $ sqlUTCTime (mkUTCTime (2018, 09, 03) (0, 0, 0)), ()))
    , (ErasedMark 1 1 55000, (Just $ sqlUTCTime (mkUTCTime (2019, 02, 02) (0, 0, 0)), ()))
    , (ErasedMark 3 3 27000, (Just $ sqlUTCTime (mkUTCTime (2018, 06, 30) (0, 0, 0)), ()))
    ]
  , iReturning = rCount
  , iOnConflict = Nothing
  }

increaseListBounty :: Int64 -> MarkID -> Update Int64
increaseListBounty amt (MarkID id) = Update
  { uTable = markTable
  , uUpdateWith = \(m, (created, _)) ->
                    ( m { markID = Just (markID m)
                        , markListBounty = markListBounty m + toFields amt
                        }
                    , (Just created, ())
                    )
  , uWhere = \(m, _) -> markID m .== toFields id
  , uReturning = rCount
  }
